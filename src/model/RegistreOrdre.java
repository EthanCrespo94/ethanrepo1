package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the registre_ordres database table.
 * 
 */
@Entity
@Table(name="registre_ordres")
@NamedQuery(name="RegistreOrdre.findAll", query="SELECT r FROM RegistreOrdre r")
public class RegistreOrdre implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	@Column(name="ordre_consola")
	private String ordreConsola;

	//bi-directional many-to-one association to Usuari
	@ManyToOne(cascade={CascadeType.PERSIST})
	@JoinColumn(name="id_usuari", referencedColumnName="id")
	private Usuari usuari;

	public RegistreOrdre() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getOrdreConsola() {
		return this.ordreConsola;
	}

	public void setOrdreConsola(String ordreConsola) {
		this.ordreConsola = ordreConsola;
	}

	public Usuari getUsuari() {
		return this.usuari;
	}

	public void setUsuari(Usuari usuari) {
		this.usuari = usuari;
	}

}