package model;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

public class Principal {


	public static void main(String[] args) {
		System.out.println("Introdueix el nom de l'usuari: ");
		Scanner input = new Scanner(System.in);
		String nom = input.next();
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("PT23_JPA_Crespo");
		EntityManager em = factory.createEntityManager();
		Usuari usr = null;
		List<RegistreOrdre> reg = null;
		em.getTransaction().begin();
		try{
			Query q = em.createQuery("Select u FROM Usuari u where u.nom LIKE '"+nom+"';");
			usr = (Usuari) q.getSingleResult();
			usr.setRegistreOrdres(reg);
		}catch(Exception e){
			usr = new Usuari();
			Query q = em.createQuery("Select MAX(u.id) from Usuari u;");
			int id = (int) q.getSingleResult();
			usr = (Usuari) q.getResultList();
			usr.setHomeDir("/home/"+usr);
			usr.setNom(nom);
			usr.setId(++id);
			reg = new ArrayList<RegistreOrdre>();
			usr.setRegistreOrdres(reg);
			em.persist(usr);
			String ordre;
			do{
				System.out.println("Introdueix una ordre: ");
				ordre=input.nextLine();
				if(ordre.equals("history"))
					System.out.println();
			}while(ordre.equals("exit")==false);
		}
		em.close();
		factory.close();
	}

}
