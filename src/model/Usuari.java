package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the usuaris database table.
 * 
 */
@Entity
@Table(name="usuaris")
@NamedQuery(name="Usuari.findAll", query="SELECT u FROM Usuari u")
public class Usuari implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="home_dir")
	private String homeDir;

	private String nom;
	@Id
	private int id;
	//bi-directional many-to-one association to RegistreOrdre
	@OneToMany(mappedBy="usuari", cascade={CascadeType.PERSIST})
	private List<RegistreOrdre> registreOrdres;

	public Usuari() {
	}
	public int getId(){
		return this.id;
	}
	public void setId(int id){
		this.id=id;
	}
	public String getHomeDir() {
		return this.homeDir;
	}

	public void setHomeDir(String homeDir) {
		this.homeDir = homeDir;
	}

	public String getNom() {
		return this.nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public List<RegistreOrdre> getRegistreOrdres() {
		return this.registreOrdres;
	}

	public void setRegistreOrdres(List<RegistreOrdre> registreOrdres) {
		this.registreOrdres = registreOrdres;
	}

	public RegistreOrdre addRegistreOrdre(RegistreOrdre registreOrdre) {
		getRegistreOrdres().add(registreOrdre);
		registreOrdre.setUsuari(this);

		return registreOrdre;
	}

	public RegistreOrdre removeRegistreOrdre(RegistreOrdre registreOrdre) {
		getRegistreOrdres().remove(registreOrdre);
		registreOrdre.setUsuari(null);

		return registreOrdre;
	}

}